var http = require("http");
var fm = require("./filemanager");
var news = {};

var date_last_message_announced_path = "./date_last_message_announced.txt";
var date_last_message_announced = 0;

news.init = function() {
	news.update_last_date();
}

news.update_last_date = function() {
	fm.create_if_doesnt_exist(date_last_message_announced_path);

	fm.read_text_from_file(date_last_message_announced_path, function(data) {
		date_last_message_announced = parseInt(data);
	});	
}

news.get_news = function(callback) {
    var req_options = {
        host: 'api.steampowered.com',
        path: '/ISteamNews/GetNewsForApp/v0002/?appid=368180&count=3&maxlength=300'
    }

    var req = http.get(req_options, function(result) {
        var bodyChunks = [];
        result.on('data', function(chunk) {
            bodyChunks.push(chunk);
        }).on('end', function() {
            var body = Buffer.concat(bodyChunks);
            news.parse_news(body, callback);
        });
    });
}

var date_format_options = {
	weekday: "long", year: "numeric", month: "short", day: "numeric"
};

// returns an array of new post objects
news.parse_news = function(body, callback) {
	// we have the latest 3 posts
	// look at each date value, if it's greater than date_last_message_announced, add message data to announce queue
		// message data: jsonData.newsItems[x].title, .author, .contents, .url
	var theobject = JSON.parse(body);
	var posts = theobject.appnews.newsitems;
	var posts_to_send = [];
	news.update_last_date();
	var new_latest_date = date_last_message_announced;
	var tempDate = new Date(0);

	for (i = 0; i < posts.length; i++) {
		var current_post = posts[i];

		if (current_post.date > date_last_message_announced) {
			tempDate = new Date(0);
			tempDate.setUTCSeconds(current_post.date);

			posts_to_send.push({
				title: current_post.title,
				url: current_post.url,
				author: current_post.author,
				contents: current_post.contents,
				date: tempDate.toLocaleTimeString('en-us', date_format_options)
			});

			if (current_post.date > new_latest_date) {
				new_latest_date = current_post.date;
			}
		}
	}

	// posts_to_send.sort(function(a, b) {
	// 	if (a.date < b.date)
	// 		return -1;
	// 	else if (a.date > b.date) 
	// 		return 1;
	// 	else
	// 		return 0;

	// });
	date_last_message_announced = new_latest_date;
	fm.write_text_to_file("" + date_last_message_announced, date_last_message_announced_path);
	callback(posts_to_send);

}

module.exports = news;