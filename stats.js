var http = require("http");
var stats = {};

stats.current_playercount = 0;
stats.playercount_inflection = "s";

stats.get_playercount = function(callback) {
    var req_options = {
        host: 'api.steampowered.com',
        path: '/ISteamUserStats/GetNumberOfCurrentPlayers/v0001?appid=368180'
    }

    var req = http.get(req_options, function(result) {
        var bodyChunks = [];
        result.on('data', function(chunk) {
            bodyChunks.push(chunk);
        }).on('end', function() {
            var body = Buffer.concat(bodyChunks);
            current_playercount = JSON.parse(body).response.player_count;
            playercount_inflection = (parseInt(current_playercount) == 1) ? "" : "s";
            callback();
        });
    });
}


module.exports = stats;