var cmds = {};

cmds.standard = {
    "help": {
        type: "reply",
        reply: "Common: `!roadmap`, `!leaderboard`, `!steam`\nFull list here: http://pastebin.com/p5TCCvzv",
    },
    "roadmap": {
        type: "reply",
        reply: "http://polyball.studiomonolith.ca/roadmap"
    },
    "rm": {
        type: "reply",
        reply: "http://polyball.studiomonolith.ca/roadmap"
    },
    "leaderboard": {
        type: "reply",
        reply: "http://polyball.studiomonolith.ca/leaderboards"
    },
    "leaderboards": {
        type: "reply",
        reply: "http://polyball.studiomonolith.ca/leaderboards"
    },
    "lb": {
        type: "reply",
        reply: "http://polyball.studiomonolith.ca/leaderboards"
    },
    "steam": {
        type: "reply",
        reply: "http://store.steampowered.com/app/368180/"
    },
    "twitter": {
        type: "reply",
        reply: "https://twitter.com/studio_monolith"
    },
}

module.exports = cmds;

// var cmds = {
//     "help": {
//         type: "reply",
//         reply: "Common: `!roadmap`, `!leaderboard`, `!steam`\nFull list here: http://pastebin.com/p5TCCvzv",
//     },
//     "roadmap": {
//         type: "reply",
//         reply: "http://polyball.studiomonolith.ca/roadmap"
//     },
//     "rm": {
//         type: "reply",
//         reply: "http://polyball.studiomonolith.ca/roadmap"
//     },
//     "leaderboard": {
//         type: "reply",
//         reply: "http://polyball.studiomonolith.ca/leaderboards"
//     },
//     "leaderboards": {
//         type: "reply",
//         reply: "http://polyball.studiomonolith.ca/leaderboards"
//     },
//     "lb": {
//         type: "reply",
//         reply: "http://polyball.studiomonolith.ca/leaderboards"
//     },
//     "steam": {
//         type: "reply",
//         reply: "http://store.steampowered.com/app/368180/"
//     },
//     "twitter": {
//         type: "reply",
//         reply: "https://twitter.com/studio_monolith"
//     },
//     "announce": {
//         type: "announce",
//         reply: function(msg) {
//             return msg.split('announce')[1];        // lmao good programming
//         }
//     }
// }
//
// module.exports = cmds;
