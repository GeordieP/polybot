var DiscordClient = require('discord.io');
// var http = require("http");
var cmds = require("./cmds");
var stats = require("./stats");
var news = require("./news");

var bot = new DiscordClient({
    autorun: true,
    //OR
    token: "MTM0NTU1NjU0MzM2Njc1ODQw.CYWcGQ.v2Aw1SQE2AH3Gdi-6IXnzPrK8Ko"
});


var refresh_interval = 2 * (60 * 1000);
var refresh_interval_timer = null;


var annChannels = [];
var controlChannels = [];

var myChannel = function() {
    this.serverID = "";          // server the channel belongs to (bot.servers[serverID])
    this.channelID = "";        // channel id (bot.servers[serverID].channels[channelID])
};

var CreateMyChannel = function(srvId, chanId) {
    myChannel.call(this);
    this.serverID = srvId;
    this.channelID = chanId;
}

CreateMyChannel.prototype = Object.create(myChannel.prototype);
CreateMyChannel.prototype.constructor = CreateMyChannel;

bot.on('ready', function() {

    bot.setPresence({game: null});

    // check_data();

    // stats.get_playercount(function() {
    //     bot.setPresence({game: current_playercount + " player" + playercount_inflection + " in-game"});
    // });

    news.init();
    check_data();

    refresh_interval_timer = setInterval(check_data, refresh_interval);

    // console.log(bot.username + " - (" + bot.id + ")");

    var srvs = Object.keys(bot.servers);
    
    for (i = 0; i < srvs.length; i++) {
        var srv = bot.servers[srvs[i]];
        var chans = Object.keys(srv.channels);
        for (j = 0; j < chans.length; j++) {
            var currChan = srv.channels[chans[j]];
            if (currChan) {     // check for undefined
                if (currChan.name === "announcements" && currChan.type === "text") {
                    annChannels.push(new CreateMyChannel(srv.id, currChan.id));

                } else if (currChan.name === "polybot-control" && currChan.type === "text") {
                    controlChannels.push(new CreateMyChannel(srv.id, currChan.id));
                }
            }
        }
    }
});

bot.on('message', function(user, userID, channelID, message, rawEvent) {
    if (message[0] === "!") {
        // check if message came from a control channel
        var cmdType = "standard";
        for (i = 0; i < controlChannels.length; i++) {
            if (channelID === controlChannels[i].channelID) {
                cmdType = "control";
                break;
            }
        }
        proc_cmd(cmdType, user, userID, channelID, message, rawEvent);
    }
});

function proc_cmd(type, user, userID, channelID, message, rawEvent) {
    var cmd_args = message.split('!')[1].split(' ');    // ends up looking like ["announce", "here", "is", "the", "announce", "message"] for !announce here is the announce message

    /*--- Control commands ---*/

    if (type === "control") {
        // check each control command individually as they do something specific and need to access things outside the cmds.js scope
        switch(cmd_args[0]) {
            case "announce":
                announce(message, channelID);
                break;
            case "check":
                // update_playercount();
                // news.get_news(function(data) {
                //     console.log("got news, data length" + data.length);
                // });
                news.get_news(announce_news);

                break;

        }
        return;
    }

    /*--- Standard Commands ---*/

    if (cmds.standard[cmd_args[0]])
        var cmd = cmds.standard[cmd_args[0]];

    if (!cmd) {
        bot.sendMessage({
            to: channelID,
            message: "Error: No command matching `" + cmd_args[0] + "` found"
        });
        return;
    }

    bot.sendMessage({
        to: channelID,
        message: cmd.reply
    });
}


// todo: function specifically for announcing news:
    // - call news.get_news(), it should return an array of objects

function announce_news(news_data) {
    // build a message to send to announce
    for (i = 0; i < news_data.length; i++) {
        var current_news_post = news_data[i];
        var msg = "New Announcement!\n\n__**" + current_news_post.title + "** posted by **" + current_news_post.author + "** on " + current_news_post.date + "__";
        msg += "\n\n" + current_news_post.contents + "\n\n" + current_news_post.url;

        for (j = 0; j < annChannels.length; j++) {
            bot.sendMessage({
                to: annChannels[j].channelID,
                message: msg
            });
        }
    }
}

function announce(message, channelID) {
    for (i = 0; i < annChannels.length; i++) {
        bot.sendMessage({
            to: annChannels[i].channelID,
            message: message.split('announce ')[1]
        });
    }
}

function check_data() {
    stats.get_playercount(function() {
        bot.setPresence({game: current_playercount + " player" + playercount_inflection + " in-game"});
    });

    news.get_news(announce_news);
}

var traverse = function(obj) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            console.log("[TRV]: " + property);
        }
    }
}


/*

https://steamcommunity.com/dev
https://developer.valvesoftware.com/wiki/Steam_Web_API
https://partner.steamgames.com/documentation/webapi

    Todo: 
        - player count (show in !status command and in game status display)
        - periodically check for new news posts

*/