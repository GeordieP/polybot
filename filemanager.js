var fs = require('fs');
var fm = {};

fm.create_if_doesnt_exist = function(path) {
	if (fs.existsSync(path)) return;

	fs.writeFile(path, "", (err) => {
		if (err) throw err;
	});
}

fm.read_json_from_file = function(path, callback) {
	if (fs.existsSync(path)) {
		fs.readFile(path, (err, data) => {
			if (err) throw err;

			callback(JSON.parse(data));
		});
	}
}

fm.write_json_to_file = function(json, path) {
	fs.writeFile(path, json, (err) => {
		if (err) throw err;
	});

}

fm.read_text_from_file = function(path, callback) {
	if (fs.existsSync(path)) {
		fs.readFile(path, (err, data) => {
			if (err) throw err;

			callback(data);
		})
	}
}

fm.write_text_to_file = function(text, path) {
	fs.writeFile(path, text, (err) => {
		if (err) throw err;
	});
}

module.exports = fm;